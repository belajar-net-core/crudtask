using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using crudtask.DTOs;
using crudtask.Entities;
using crudtask.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace crudtask.Controllers
{
    [ApiController]
    [Route("api/tasks")]
    public class UserController : ControllerBase
    {
        private readonly MainRepository _repo;

        public UserController(MainRepository repo)
        {
            _repo = repo;
        }

        [HttpPost("AddUserWithTask")]
        public IActionResult AddUser(UserDTO dto)
        {
            try
            {
                var result = _repo.InsertUser(dto);
                return Ok(result);
            }
            catch (Exception e)
            {
                return Problem(e.Message);
            }
        }

        [HttpGet("/GetUsersWithTask")]
        public IActionResult GetUsers()
        {
            try
            {
                var result = _repo.GetUsers();
                return Ok(result);
            }
            catch (System.Exception)
            {
                return Problem("Server error");
            }
        }

        [HttpGet("/GetUserWithTask")]
        public IActionResult FindUser([FromQuery] string name)
        {
            try
            {
                var result = _repo.FindUser(name);
                if (result.Count < 1)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (System.Exception)
            {
                return Problem("Server error");
            }
        }

    }
}