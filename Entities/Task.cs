using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace crudtask.Entities
{
    public class Task
    {
        [JsonPropertyName("pk_task_id")]
        public int Id { get; set; }

        [JsonPropertyName("task_detail")]
        public string Detail { get; set; } = string.Empty;

        [JsonPropertyName("fk_user_id")]
        public int UserId { get; set; }

        [JsonIgnore]
        public User? User { get; set; }
    }
}