using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace crudtask.Entities
{
    public class User
    {
        [JsonPropertyName("pk_user_id")]
        public int Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; } = string.Empty;

        [JsonPropertyName("tasks")]
        [JsonPropertyOrder(1)]
        public IEnumerable<Task> Tasks { get; set; } = new List<Task>();
    }
}