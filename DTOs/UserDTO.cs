using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace crudtask.DTOs
{
    public class UserDTO
    {
        [JsonPropertyName("name")]
        public string Name { get; set; } = string.Empty;

        [JsonPropertyName("tasks")]
        public List<TaskDTO> Tasks { get; set; } = new List<TaskDTO>();
    }
}