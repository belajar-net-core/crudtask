using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace crudtask.DTOs
{
    public class TaskDTO
    {
        [JsonPropertyName("task_detail")]
        public string Detail { get; set; } = string.Empty;
    }
}