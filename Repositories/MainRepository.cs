using System.Collections.ObjectModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using crudtask.DTOs;
using crudtask.Entities;

namespace crudtask.Repositories
{
    public class MainRepository
    {
        private readonly IConfiguration _config;

        private readonly string ConnectionString;

        private readonly ILogger<MainRepository> _logger;

        public MainRepository(IConfiguration config, ILogger<MainRepository> logger)
        {
            _config = config;
            _logger = logger;
            this.ConnectionString = _config.GetConnectionString("DefaultConnection");
        }

        public int InsertUserQueryBuilder(SqlConnection conn, SqlTransaction transaction, UserDTO dto)
        {
            const string query = "INSERT INTO Users (name) VALUES (@Name); SELECT SCOPE_IDENTITY()";
            SqlCommand cmd = new(query, conn, transaction);
            cmd.Parameters.AddWithValue("@Name", dto.Name);
            int userId = Convert.ToInt32(cmd.ExecuteScalar());
            return userId;
        }

        public void BulkInsertTasksQueryBuilder(SqlConnection conn, SqlTransaction transaction, List<TaskDTO> tasks, int userId)
        {
            DataTable dataTable = new();
            dataTable.Columns.Add("TaskDetail", typeof(string));
            dataTable.Columns.Add("FkUserId", typeof(int));

            foreach (var task in tasks)
            {
                dataTable.Rows.Add(task.Detail, userId);
            }

            using SqlBulkCopy bulkCopy = new(conn, SqlBulkCopyOptions.Default, transaction);
            bulkCopy.DestinationTableName = "Tasks";
            bulkCopy.ColumnMappings.Add("TaskDetail", "task_detail");
            bulkCopy.ColumnMappings.Add("FkUserId", "fk_user_id");
            bulkCopy.WriteToServer(dataTable);
        }

        public bool InsertUser(UserDTO dto)
        {
            using (SqlConnection connection = new(ConnectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                try
                {
                    // Insert user
                    int userId = InsertUserQueryBuilder(connection, transaction, dto);

                    // Insert tasks using bulk copy
                    BulkInsertTasksQueryBuilder(connection, transaction, dto.Tasks, userId);

                    transaction.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    Console.WriteLine("Error: " + ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return false;
        }

        public ICollection<User> GetUsers()
        {
            List<User> users = new();

            using (SqlConnection conn = new(ConnectionString))
            {
                conn.Open();
                const string query = "SELECT * FROM Users";
                var cmd = new SqlCommand(query, conn);
                using SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    int userId = Convert.ToInt32(reader["pk_users_id"]);
                    User user = new()
                    {
                        Id = userId,
                        Name = reader["name"].ToString() ?? "",
                        Tasks = GetTasksForUser(userId)
                    };

                    users.Add(user);
                }
            }
            return users;
        }

        public ICollection<User> FindUser(string name)
        {
            List<User> users = new();

            using (SqlConnection conn = new(ConnectionString))
            {
                conn.Open();
                const string query = "SELECT * FROM Users WHERE name LIKE @Name";
                var cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@Name", $"%{name}%");
                using SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    int userId = Convert.ToInt32(reader["pk_users_id"]);
                    User user = new()
                    {
                        Id = userId,
                        Name = reader["name"].ToString() ?? "",
                        Tasks = GetTasksForUser(userId)
                    };

                    users.Add(user);
                }
            }
            return users;
        }

        public ICollection<Entities.Task> GetTasksForUser(int userId)
        {
            var tasks = new List<Entities.Task>();
            // _logger.LogInformation("taskUser : ", userId);
            const string query = "SELECT * FROM Tasks WHERE fk_user_id = @UserId";
            using (SqlConnection conn = new(ConnectionString))
            {
                conn.Open();
                SqlCommand cmd = new(query, conn);
                cmd.Parameters.AddWithValue("@UserId", userId);
                using SqlDataReader taskReader = cmd.ExecuteReader();
                while (taskReader.Read())
                {
                    Entities.Task task = new()
                    {
                        Id = Convert.ToInt32(taskReader["pk_tasks_id"]),
                        Detail = taskReader["task_detail"].ToString() ?? "",
                        UserId = userId
                    };
                    tasks.Add(task);
                }
                taskReader.Close();
            }
            return tasks;
        }

    }
}